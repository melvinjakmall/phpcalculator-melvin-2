<?php

namespace Jakmall\Recruitment\Calculator\Commands\History;

use Illuminate\Console\Command;

class HistoryListCommand extends Command
{
    public function __construct()
    {
        $this->signature = 'history:list {commands?* : Filter the history by commands}';
        $this->description = 'Show calculator history';

        parent::__construct();
    }

    public function handle()
    {
        $filters = $this->getInput();

        $this->printHistory($filters);
    }

    /**
     * @return array|string|null
     */
    protected function getInput()
    {
        return $this->argument('commands');
    }

    /**
     * @param $filters
     */
    protected function printHistory($filters): void
    {
        $historyList = HistoryList::getHistoryItems();
        $historyList = HistoryList::filterHistoryItems($filters, $historyList);

        if (sizeof($historyList) <= 0) {
            print("History is empty.\n");
            return;
        }
        $this->printHistoryTable($historyList);
    }

    /**
     * @param HistoryItem[] $historyList
     */
    protected function printHistoryTable($historyList)
    {
        $header = array('No', 'Command', 'Description', 'Result', 'Output', 'Time');
        $items = [];
        $no = 1;
        foreach ($historyList as $historyItem) {
            $items[] = array(
                $no++,
                $historyItem->getName(),
                $historyItem->getDescription(),
                $historyItem->getResult(),
                $historyItem->getOutput(),
                $historyItem->getTime()
            );
        }
        $this->table($header, $items);
    }
}
