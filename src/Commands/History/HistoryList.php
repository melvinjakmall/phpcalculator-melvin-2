<?php

namespace Jakmall\Recruitment\Calculator\Commands\History;

class HistoryList
{
    /**
     * @var string
     */
    protected static $FILE_NAME = 'history.txt';

    /**
     * @var string
     */
    private static $separator = "\n";

    /**
     * @param $commandName
     * @param $numbersList
     * @param $calculationResult
     */
    public static function addToHistory($commandName, $numbersList, $calculationResult)
    {
        $newHistoryItem = new HistoryItem($commandName, $numbersList, $calculationResult);

        $historyItems = static::getHistoryItemsFromFile();
        $historyItems[] = $newHistoryItem;

        static::saveHistoryItemsToFile($historyItems);
    }

    /**
     * @return  HistoryItem[]
     */
    protected static function getHistoryItemsFromFile()
    {
        $historyItemsList = [];

        static::createFileIfNotExist();

        $fileContent = file_get_contents(static::$FILE_NAME);
        if (strlen($fileContent) <= 0) {
            return $historyItemsList;
        }

        $fileContentItems = explode(static::$separator, $fileContent);

        foreach ($fileContentItems as $fileContentItemString) {
            $historyItemsList[] = HistoryItem::decode($fileContentItemString);
        }

        return $historyItemsList;
    }

    /**
     * @param               $filters
     * @param HistoryItem[] $historyList
     *
     * @return HistoryItem[]
     */
    public static function filterHistoryItems($filters, $historyList)
    {
        if ($filters == null || sizeof($filters) == 0) {
            return $historyList;
        }

        foreach ($historyList as $index => $historyItem) {
            if (!in_array($historyItem->getName(), $filters)) {
                unset($historyList[$index]);
            }
        }

        return $historyList;
    }

    protected static function createFileIfNotExist(): void
    {
        file_put_contents(static::$FILE_NAME, '', FILE_APPEND);
    }

    protected static function saveHistoryItemsToFile($historyItems): void
    {
        file_put_contents(static::$FILE_NAME, implode(static::$separator, $historyItems));
    }

    /**
     * @return HistoryItem[]
     */
    public static function getHistoryItems()
    {
        return static::getHistoryItemsFromFile();
    }

    public static function clearHistory(): void
    {
        static::clearHistoryFile();
    }

    protected static function clearHistoryFile(): void
    {
        file_put_contents(static::$FILE_NAME, '');
    }
}
