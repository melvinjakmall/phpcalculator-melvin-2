<?php

namespace Jakmall\Recruitment\Calculator\Commands\History;

use Illuminate\Console\Command;

class HistoryClearCommand extends Command
{
    public function __construct()
    {
        $this->signature = 'history:clear';
        $this->description = 'Clear saved history';

        parent::__construct();
    }

    /**
     * @return string
     */
    public function getCommandName()
    {
        return 'history:clear';
    }

    public function handle()
    {
        HistoryList::clearHistory();

        print("History cleared!\n");
    }
}
