<?php

namespace Jakmall\Recruitment\Calculator\Commands\Calculation\Add;

use Jakmall\Recruitment\Calculator\Commands\Calculation\AbstractBasicCalculationCommand;

class AddCommand extends AbstractBasicCalculationCommand
{
    /**
     * @return void
     */
    protected function initializeCalculatorOperator(): void
    {
        $this->calculatorOperator = new AddOperator();
    }
}
