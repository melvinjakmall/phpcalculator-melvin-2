<?php

namespace Jakmall\Recruitment\Calculator\Commands\Calculation;

interface CalculationCommandInterface
{
    /**
     * @param int|float $number1
     * @param int|float $number2
     *
     * @return int|float
     */
    public function calculate($number1, $number2);

    /**
     * @return string
     */
    public function getOperator(): string;

    /**
     * @return string
     */
    public function getCommandVerb(): string;

    /**
     * @return string
     */
    public function getCommandPassiveVerb(): string;
}
