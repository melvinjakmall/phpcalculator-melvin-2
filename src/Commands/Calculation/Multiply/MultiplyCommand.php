<?php

namespace Jakmall\Recruitment\Calculator\Commands\Calculation\Multiply;

use Jakmall\Recruitment\Calculator\Commands\Calculation\AbstractBasicCalculationCommand;

class MultiplyCommand extends AbstractBasicCalculationCommand
{
    /**
     * @return void
     */
    protected function initializeCalculatorOperator(): void
    {
        $this->calculatorOperator = new MultiplyOperator();
    }
}
