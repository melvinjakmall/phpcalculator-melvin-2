<?php

namespace Jakmall\Recruitment\Calculator\Commands\Calculation\Pow;

use Jakmall\Recruitment\Calculator\Commands\Calculation\CalculationCommandInterface;

class PowOperator implements CalculationCommandInterface
{
    /**
     * @param int|float $number1
     * @param int|float $number2
     *
     * @return int|float
     */
    public function calculate($number1, $number2)
    {
        return pow($number1, $number2);
    }

    /**
     * @return string
     */
    public function getOperator(): string
    {
        return '^';
    }

    /**
     * @return string
     */
    public function getCommandVerb(): string
    {
        return 'exponent';
    }

    /**
     * @return string
     */
    public function getCommandPassiveVerb(): string
    {
        return 'exponent';
    }
}
