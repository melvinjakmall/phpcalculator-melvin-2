<?php

namespace Jakmall\Recruitment\Calculator\Commands\Calculation\Pow;

use Jakmall\Recruitment\Calculator\Commands\Calculation\AbstractCalculationCommand;

class PowCommand extends AbstractCalculationCommand
{
    public function __construct()
    {
        parent::__construct();
    }

    public function initializeCommandSignature(): void
    {
        $this->signature = sprintf(
            '%s {base : The base number}{exp : The exponent number}',
            $this->getCommandName(),
        );
    }

    public function initializeCommandDescription(): void
    {
        $this->description = sprintf('%s the given Numbers', ucfirst($this->calculatorOperator->getCommandVerb()));
    }

    /**
     * @return void
     */
    protected function initializeCalculatorOperator(): void
    {
        $this->calculatorOperator = new PowOperator();
    }

    /**
     * @return string
     */
    public function getCommandName(): string
    {
        return 'pow';
    }
}
