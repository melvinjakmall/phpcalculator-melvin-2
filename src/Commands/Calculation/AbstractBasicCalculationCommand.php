<?php

namespace Jakmall\Recruitment\Calculator\Commands\Calculation;

abstract class AbstractBasicCalculationCommand extends AbstractCalculationCommand
{
    public function __construct()
    {
        parent::__construct();
    }

    public function initializeCommandSignature(): void
    {
        $this->signature = sprintf(
            '%s {numbers* : The numbers to be %s}',
            $this->getCommandName(),
            $this->calculatorOperator->getCommandPassiveVerb()
        );
    }

    public function initializeCommandDescription(): void
    {
        $this->description = sprintf('%s all given Numbers', ucfirst($this->calculatorOperator->getCommandVerb()));
    }

    /**
     * @return string
     */
    public function getCommandName(): string
    {
        return $this->calculatorOperator->getCommandVerb();
    }
}
