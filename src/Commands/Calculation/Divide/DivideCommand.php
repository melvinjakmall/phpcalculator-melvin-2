<?php

namespace Jakmall\Recruitment\Calculator\Commands\Calculation\Divide;

use Jakmall\Recruitment\Calculator\Commands\Calculation\AbstractBasicCalculationCommand;

class DivideCommand extends AbstractBasicCalculationCommand
{
    /**
     * @return void
     */
    protected function initializeCalculatorOperator(): void
    {
        $this->calculatorOperator = new DivideOperator();
    }
}
