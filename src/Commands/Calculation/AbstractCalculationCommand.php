<?php

namespace Jakmall\Recruitment\Calculator\Commands\Calculation;

use Illuminate\Console\Command;
use Jakmall\Recruitment\Calculator\Commands\History\HistoryList;

abstract class AbstractCalculationCommand extends Command
{
    /**
     * @var string
     */
    protected $signature;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var CalculationCommandInterface
     */
    protected $calculatorOperator;

    public function __construct()
    {
        $this->initializeCalculatorOperator();
        $this->initializeCommandSignature();
        $this->initializeCommandDescription();

        parent::__construct();
    }

    public function handle()
    {
        $numbers = $this->getInput();
        $numbersList = $this->glueNumbersList($numbers);
        $calculationResult = $this->calculateAll($numbers);

        $this->generateCalculationDescription($numbersList, $calculationResult);
        HistoryList::addToHistory($this->getCommandName(), $numbersList, $calculationResult);
    }

    /**
     * @return array
     */
    protected function getInput()
    {
        $inputs = [];
        $arguments = $this->arguments();
        array_splice($arguments, 0, 1);

        foreach ($arguments as $arg) {
            if (!is_array($arg)) {
                $inputs[] = $arg;
            } else {
                foreach ($arg as $a) {
                    $inputs[] = $a;
                }
            }
        };
        return $inputs;
    }

    /**
     * @param array $numbers
     * @return string
     */
    protected function glueNumbersList(array $numbers)
    {
        $glue = sprintf(' %s ', $this->calculatorOperator->getOperator());
        $numbersList = implode($glue, $numbers);
        return $numbersList;
    }

    /**
     * @param string    $numbersList
     * @param           $result
     */
    protected function generateCalculationDescription($numbersList, $result)
    {
        $combinedDescription = sprintf('%s = %s', $numbersList, $result);
        $this->comment($combinedDescription);
    }

    /**
     * @param array $numbers
     *
     * @return float|int
     */
    protected function calculateAll(array $numbers)
    {
        $number = array_pop($numbers);
        if (count($numbers) <= 0) {
            return $number;
        }
        return $this->calculatorOperator->calculate($this->calculateAll($numbers), $number);
    }

    /**
     * @return string
     */
    abstract public function getCommandName(): string;

    abstract protected function initializeCalculatorOperator(): void;

    abstract protected function initializeCommandSignature(): void;

    abstract protected function initializeCommandDescription(): void;
}
