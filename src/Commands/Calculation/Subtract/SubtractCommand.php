<?php

namespace Jakmall\Recruitment\Calculator\Commands\Calculation\Subtract;

use Jakmall\Recruitment\Calculator\Commands\Calculation\AbstractBasicCalculationCommand;

class SubtractCommand extends AbstractBasicCalculationCommand
{
    /**
     * @return void
     */
    protected function initializeCalculatorOperator(): void
    {
        $this->calculatorOperator = new SubtractOperator();
    }
}
