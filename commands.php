<?php

use Jakmall\Recruitment\Calculator\Commands\Calculation\Add\AddCommand;
use Jakmall\Recruitment\Calculator\Commands\Calculation\Divide\DivideCommand;
use Jakmall\Recruitment\Calculator\Commands\Calculation\Multiply\MultiplyCommand;
use Jakmall\Recruitment\Calculator\Commands\Calculation\Pow\PowCommand;
use Jakmall\Recruitment\Calculator\Commands\Calculation\Subtract\SubtractCommand;
use Jakmall\Recruitment\Calculator\Commands\History\HistoryClearCommand;
use Jakmall\Recruitment\Calculator\Commands\History\HistoryListCommand;

return [
    AddCommand::class,
    SubtractCommand::class,
    MultiplyCommand::class,
    DivideCommand::class,
    PowCommand::class,
    HistoryListCommand::class,
    HistoryClearCommand::class
];
